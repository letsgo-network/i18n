package i18n

import "github.com/gohouse/e"

type IParser interface {
	SetOptions(opts *Options)
	Parse() e.Error
	Load(key string, defaultVal ...string) interface{}
}
