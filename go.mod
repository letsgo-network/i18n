module github.com/gohouse/i18n

go 1.12

require github.com/gohouse/e v0.0.1

replace github.com/gohouse/e => ../e
